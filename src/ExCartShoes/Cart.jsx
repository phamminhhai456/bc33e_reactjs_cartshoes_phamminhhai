import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} alt="" width={100} />
          </td>
          <td>
            <button
              onClick={() => this.props.handleChangeQuantity(item.id, 1)}
              className="btn btn-success"
            >
              +
            </button>
            <span className="px-2">{item.quantity}</span>
            <button
              onClick={() => this.props.handleChangeQuantity(item.id, -1)}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
          <td>{item.quantity * item.price}</td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Giá</th>
            <th>Hình Ảnh</th>
            <th>Số Lượng</th>
            <th>Thành Tiền</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
