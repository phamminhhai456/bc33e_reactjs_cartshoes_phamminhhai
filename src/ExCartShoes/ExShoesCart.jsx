import React, { Component } from "react";
import { data_shoes } from "./DataShoe";
import ItemShoe from "./ItemShoe";
import Cart from "./Cart";
export default class ExShoesCart extends Component {
  state = {
    shoes: data_shoes,
    cart: [],
  };

  renderContent = () => {
    return this.state.shoes.map((item, index) => {
      return (
        <ItemShoe
          handleAddToCart={this.handleAddToCart}
          data={item}
          key={index}
        />
      );
    });
  };

  handleAddToCart = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });

    let cloneCart = [...this.state.cart];

    if (index === -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }
    this.setState({ cart: cloneCart });
  };

  handleChangeQuantity = (idShoe, value) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id === idShoe;
    });

    let cloneCart = [...this.state.cart];
    if (index === -1) {
      return;
    }

    cloneCart[index].quantity += value;
    cloneCart[index].quantity === 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };

  render() {
    return (
      <div className="container">
        <Cart
          handleChangeQuantity={this.handleChangeQuantity}
          cart={this.state.cart}
        />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
