import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3 mt-5">
        <div className="card h-100" style={{ width: "100%" }}>
          <img className="w-100 card-img-top" src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-secondary"
            >
              Add To Cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
