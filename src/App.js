// import logo from "./logo.svg";
import "./App.css";
import ExShoesCart from "./ExCartShoes/ExShoesCart";

function App() {
  return (
    <div className="App">
      <ExShoesCart />
    </div>
  );
}

export default App;
